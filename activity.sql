SELECT name FROM artists 
WHERE name LIKE "%D%";

SELECT * 
FROM songs
WHERE length < 230;

SELECT albums.name, songs.title, songs.length 
FROM albums 
JOIN songs 
ON albums.id = songs.album_id;

SELECT * FROM artists 
JOIN albums 
ON artists.id = albums.artist_id 
WHERE albums.name LIKE "%A%";

SELECT * FROM albums 
WHERE id > 18 
ORDER BY id DESC;

SELECT * FROM albums 
JOIN songs ON albums.id = songs.album_id 
ORDER BY albums.name DESC, songs.title ASC;
